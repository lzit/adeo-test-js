const { expect } = require('chai');
const sinon = require('sinon');

const runProcess = require('../../src/helpers/runProcess');

describe('runProcess', () => {
  before(() => {
    sinon.stub(process, 'exit');
  });

  after(() => {
    process.exit.restore();
  });

  it('should exit with 0 if run succeed', () => {
    const argv = ['node', 'app.js', '--filter=An'];
    runProcess(argv, []);

    sinon.assert.calledWith(process.exit, 0);
  });

  it('should exit with 1 if filter pattern is not provided', () => {
    const argv = ['node', 'app.js'];
    runProcess(argv, []);

    sinon.assert.calledWith(process.exit, 1);
  });
});
