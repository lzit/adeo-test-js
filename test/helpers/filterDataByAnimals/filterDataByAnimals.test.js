const { expect } = require('chai');
const dataFixture = require('./data.fixtures');
const expectedResultFilterByAn = require('./expectedResultWithFilterByAn.fixture');

const filterDataByAnimals = require('../../../src/helpers/filterDataByAnimals');

describe('filterDataByAnimals', () => {
  it('should return data with animals corresponding to filter pattern', () => {
    const result = filterDataByAnimals(dataFixture, 'An');
    expect(result).to.deep.equal(expectedResultFilterByAn);
  });

  it('should throw an error if data is not an array', () => {
    const badData = { name: 'name', people: [] };
    expect(() => filterDataByAnimals(badData, 'An')).to.throw(
      Error,
      /Data must be an array/
    );
  });

  it('should not raise an error if a city does not contain people', () => {
    const dataWithMissingPeople = [{ ...dataFixture[0] }];
    delete dataWithMissingPeople[0].people;
    expect(() => filterDataByAnimals(dataWithMissingPeople, 'An')).to.not.throw(
      Error
    );
  });

  it('should not raise an error if a person does not contain animals', () => {
    const dataWithMissingAnimals = [{ ...dataFixture[0] }];
    delete dataWithMissingAnimals[0].people[0].animals;
    expect(() =>
      filterDataByAnimals(dataWithMissingAnimals, 'An')
    ).to.not.throw(Error);
  });
});
