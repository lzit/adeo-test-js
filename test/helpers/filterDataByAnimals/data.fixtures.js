const data = [
  {
    name: 'Dillauti',
    people: [
      {
        name: 'Winifred Graham',
        animals: [
          { name: 'Anoa' },
          { name: 'Duck' },
          { name: 'NarwhalAn' },
          { name: 'Badgeran' },
          { name: 'Cobra' },
          { name: 'Crow' },
        ],
      },
      {
        name: 'Blanche Viciani',
        animals: [
          { name: 'Barbet' },
          { name: 'Rhea' },
          { name: 'Snakes' },
          { name: 'Antelope' },
          { name: 'Echidna' },
          { name: 'Crow' },
          { name: 'Guinea Fowl' },
          { name: 'Deer Mouse' },
        ],
      },
    ],
  },
  {
    name: 'Tohabdal',
    people: [
      {
        name: 'Effie Houghton',
        animals: [
          { name: 'Zebra' },
          { name: 'Ring-tailed Lemur' },
          { name: 'Fly' },
          { name: 'Blue Iguana' },
          { name: 'AnEmu' },
          { name: 'African Wild Ass' },
          { name: 'Numbat' },
        ],
      },
      {
        name: 'Essie Bennett',
        animals: [
          { name: 'Aldabra Tortoise' },
          { name: 'Patagonian Toothfish' },
          { name: 'Giant Panda' },
          { name: 'Goat' },
          { name: 'Quahog' },
          { name: 'Collared Lemur' },
          { name: 'Aldabra Tortoise' },
        ],
      },
    ],
  },
  {
    name: 'Zuhackog',
    people: [
      {
        name: 'Elva Baroni',
        animals: [
          { name: 'Silkworm' },
          { name: 'Zebu' },
          { name: 'King Vulture' },
          { name: 'Zebrashark' },
          { name: 'Ostrich' },
          { name: 'Waxwing' },
        ],
      },
      {
        name: 'Johnny Graziani',
        animals: [
          { name: 'Dunnart' },
          { name: 'Cotinga' },
          { name: 'Carp' },
          { name: 'Bat' },
          { name: 'Olive Sea Snake' },
          { name: 'Caterpillar' },
          { name: 'Jackal' },
        ],
      },
      {
        name: 'Herman Christensen',
        animals: [
          { name: 'Death Adder' },
          { name: 'Pronghorn' },
          { name: 'Carp' },
          { name: 'Jaguar' },
          { name: 'Anteater' },
          { name: 'Zebu' },
          { name: 'Red Ruffed Lemur' },
        ],
      },
      {
        name: 'Fannie Ancillotti',
        animals: [
          { name: 'Silkworm' },
          { name: 'Horses' },
          { name: 'Anaconda' },
          { name: 'Guinea' },
          { name: 'Bird' },
          { name: 'Aardwolf' },
          { name: 'Crane Fly' },
          { name: 'Caterpillar' },
        ],
      },
      {
        name: 'Lawrence Camiciottoli',
        animals: [
          { name: 'Bustard' },
          { name: 'Numbat' },
          { name: 'Cat' },
          { name: 'Gecko' },
          { name: 'Northern Red Snapper' },
          { name: 'Monkfish' },
          { name: 'Birds' },
          { name: 'Caterpillar' },
          { name: 'Mule' },
        ],
      },
      {
        name: 'Marion Landi',
        animals: [
          { name: 'Tortoise' },
          { name: 'Mule' },
          { name: 'Hedgehog' },
          { name: 'Geckos' },
          { name: 'Sheep' },
          { name: 'Emu' },
        ],
      },
      {
        name: 'Lou de Bruin',
        animals: [
          { name: 'Boa' },
          { name: 'Death Adder' },
          { name: 'Okapi' },
          { name: 'Fly' },
          { name: 'Horses' },
        ],
      },
    ],
  },
];

module.exports = data;
