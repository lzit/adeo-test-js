const expectedResult = [
  {
    name: 'Dillauti [2]',
    people: [
      {
        name: 'Winifred Graham [2]',
        animals: [{ name: 'Anoa' }, { name: 'NarwhalAn' }],
      },
      {
        name: 'Blanche Viciani [1]',
        animals: [{ name: 'Antelope' }],
      },
    ],
  },
  {
    name: 'Tohabdal [1]',
    people: [
      {
        name: 'Effie Houghton [1]',
        animals: [{ name: 'AnEmu' }],
      },
    ],
  },
  {
    name: 'Zuhackog [2]',
    people: [
      {
        name: 'Herman Christensen [1]',
        animals: [{ name: 'Anteater' }],
      },
      {
        name: 'Fannie Ancillotti [1]',
        animals: [{ name: 'Anaconda' }],
      },
    ],
  },
];

module.exports = expectedResult;
