const { expect } = require('chai');

const getFilterPatternFromArguments = require('../../src/helpers/getFilterPatternFromArguments');

describe('getFilterPatternFromArguments', () => {
  it('should return filter pattern if parameter is provided', () => {
    const expectedFilterPattern = 't';
    const argv = ['node', 'app.js', `--filter=${expectedFilterPattern}`];
    const filterPattern = getFilterPatternFromArguments(argv);

    expect(filterPattern, 'Filter pattern').to.equal(expectedFilterPattern);
  });

  it('should return null if --filter is not provided', () => {
    const argv = ['node', 'app.js'];
    const filterPattern = getFilterPatternFromArguments(argv);

    expect(filterPattern).to.be.null;
  });

  it('should return null if a filter pattern is not provided', () => {
    const argv = ['node', 'app.js', '--filter'];
    const valid = getFilterPatternFromArguments(argv);

    expect(valid).to.be.null;
  });
});
