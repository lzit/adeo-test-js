/**
 *  Return data with animals filtered by filter pattern.
 * @param {Object[]} data
 * @param {string} data[].name
 * @param {Object[]} data[].people
 * @param {string} data[].people[].name
 * @param {Object[]} data[].people[].animals
 * @param {string} data[].people[].animals[].name
 * @param {string} filterPattern
 */
function filterDataByAnimals(data, filterPattern) {
  if (!Array.isArray(data)) throw new Error('Data must be an array');

  const filteredCities = data.reduce((result, city) => {
    const resultToReturn = [...result];
    const filteredCity = filterCity(city, filterPattern);
    if (filteredCity) {
      resultToReturn.push(filteredCity);
    }
    return resultToReturn;
  }, []);
  return filteredCities;
}

function filterCity(city, filterPattern) {
  let cityToReturn = null;
  if (!city.people) return cityToReturn;
  const filteredPeople = city.people.reduce((result, person) => {
    const personToReturn = filterPerson(person, filterPattern);
    if (personToReturn) {
      result.push(personToReturn);
    }
    return result;
  }, []);

  if (filteredPeople.length) {
    const newName = `${city.name} [${filteredPeople.length}]`;
    cityToReturn = { name: newName, people: filteredPeople };
  }

  return cityToReturn;
}

function filterPerson(person, filterPattern) {
  let personToReturn = null;
  if (!person.animals) return personToReturn;
  const filteredAnimals = person.animals.filter((animal) =>
    filterAnimal(animal, filterPattern)
  );

  if (filteredAnimals.length) {
    const newName = `${person.name} [${filteredAnimals.length}]`;
    personToReturn = { name: newName, animals: filteredAnimals };
  }

  return personToReturn;
}

function filterAnimal(animal, filterPattern) {
  return animal.name.includes(filterPattern);
}

module.exports = filterDataByAnimals;
