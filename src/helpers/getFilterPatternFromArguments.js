/**
 * Extract filter pattern from arguments.
 * @param {String[]} argv Process arguments
 * @returns {String | null} return the filter pattern or null if it isn't provided
 */
function getFilterPatternFromArguments(argv) {
  if (argv.length != 3) {
    return null;
  }

  const matched = /--filter=(.+)/.exec(argv[2]);
  return matched ? matched[1] : null;
}

module.exports = getFilterPatternFromArguments;
