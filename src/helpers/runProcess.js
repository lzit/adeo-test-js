const getFilterPatternFromArguments = require('./getFilterPatternFromArguments');
const filterDataByAnimals = require('./filterDataByAnimals');
const displayResults = require('./displayResult');

function run(argv, dataToProcess) {
  const filterPattern = getFilterPatternFromArguments(argv);

  if (!filterPattern) {
    console.log('Filter pattern is missing.');
    console.log('usage: node app.js --filter=[filter pattern]');
    process.exit(1);
  }

  const results = filterDataByAnimals(dataToProcess, filterPattern);

  displayResults(results);

  process.exit(0);
}

module.exports = run;
