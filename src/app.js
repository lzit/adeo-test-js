const { data } = require('../data.js');
const run = require('./helpers/runProcess');

const argv = process.argv;

run(argv, data);

module.exports = run;
